<?php
/**
 * Post Reference Control.
 *
 * @package lzb-postr
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( 'Project_Lzb_Control_postr' ) ) :
    /**
     * Project_Lzb_Control_postr class.
     *
     * LazyBlocks_Control - https://github.com/nk-o/lazy-blocks/blob/master/src/controls/_base/index.php
     */
    class Project_Lzb_Control_postr extends LazyBlocks_Control {
        /**
         * Constructor
         */
        public function __construct() {
            // Control unique name.
            $this->name = 'postr';

            // Control icon SVG.
            // You may use these icons https://material.io/resources/icons/?icon=accessibility&style=outline .
            $this->icon = '<svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24"><g><rect fill="none" height="24" width="24"/></g><g><g><rect height="2" width="9" x="13" y="7"/><rect height="2" width="9" x="13" y="15"/><rect height="2" width="6" x="16" y="11"/><polygon points="13,12 8,7 8,11 2,11 2,13 8,13 8,17"/></g></g></svg>';

            // Control value type [string, number, boolean].
            $this->type = 'string';

            // Control label.
            $this->label = __( 'Post Reference', 'project' );

            // Category name [basic, content, choice, advanced, layout]
            // How to add custom category - https://lazyblocks.com/documentation/php-filters/lzb-controls-categories/
            $this->category = 'content';

            // Add/remove some options from control settings.
            // More options see in https://github.com/nk-o/lazy-blocks/blob/master/src/controls/_base/index.php .
            $this->restrictions = array(
                'default_settings' => false,
                'help_settings'    => false,
            );

            // Optional additional attributes, that will be saved in control data.
            $this->attributes = array(
                'postr_types' => '',
            );

            parent::__construct();
        }

        /**
         * Register control assets.
         */
        public function register_assets() {
            wp_register_script(
                'Project-lzb-control-postr',
                Project_Lzb_Plugin_postr::$plugin_url . 'assets/js/postr.min.js',
                array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-components' ),
                '1.0.0',
                true
            );
            wp_register_style(
                'Project-lzb-control-postr',
                Project_Lzb_Plugin_postr::$plugin_url . 'assets/css/postr.min.css',
                array(),
                '1.0.0'
            );
        }

        /**
         * Enqueue control scripts.
         *
         * @return array script dependencies.
         */
        public function get_script_depends() {
            return array( 'Project-lzb-control-postr' );
        }

        /**
         * Enqueue control styles.
         *
         * @return array style dependencies.
         */
        public function get_style_depends() {
            return array( 'Project-lzb-control-postr' );
        }
    }

    // Init.
    new Project_Lzb_Control_postr();
endif;
