# Lazy Blocks: Post Reference Control

* Contributors: Content and Coffee
* Tags: posts, reference, id
* Requires at least: 5.0.0
* Tested up to: 5.3
* Requires PHP: 5.5.9
* Stable tag: 1.0.0
* License: GPLv2 or later
* License URI: <http://www.gnu.org/licenses/gpl-2.0.html>

Small plugin to enable a Post Reference Control inside of a Lazy Block.

## Description

Small plugin to enable a Post Reference Control inside of a Lazy Block. With this control you can search/select a post.
Then in your template or elsewhere you can then get the ID of the post selected.

## Installation

### Automatic installation

This plugin is not registered in wp.org, you need to install manually.

### Manual installation

The manual installation method involves downloading or building the lzb-postr plugin and uploading it to your project, then enabling it.


## Usage

Create a lazy block that uses the postr control.


### Block Code
```
            'control_d919e94362' => array(
                'type' => 'postr',
                'name' => 'postr',
                'default' => '',
                'label' => 'post',
                'help' => '',
                'child_of' => '',
                'placement' => 'content',
                'width' => '100',
                'hide_if_not_selected' => 'false',
                'save_in_meta' => 'false',
                'save_in_meta_name' => '',
                'required' => 'false',
                'postr_types' => 'solution_partner,page',  // Specify which types to filter on OR blank for all.
                'placeholder' => '',
                'characters_limit' => '',
                'postr_custom_attribute' => 'solution_partner',
            ),
```

### Template

In the template you have only the name of the post and the id.

Make some handlebars helpers or use PHP to load the post based on id.

```
// Handler Bars
{{controlname.id}} - show the id of the chosen post.

// PHP
<?= $attributes['controlname']['id'] ?>

// PHP more interesting...
<?php
    $postId = $attributes['controlname']['id'];
    $post = get_post($postId);
?>

<p>
    <?= $post->post_title ?>
</p>
```


