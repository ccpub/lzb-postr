# Lazy Blocks Custom Control "postr"

This is ^THE SOURCE^ of a small plugin to create a post reference control for Lazy Blocks in Wordpress.


## Development

### Installation

- Run `npm install` in the command line

### Building

- `npm run build` to run build
- `npm run dev` to run build and start files watcher

### Wordpress Installation

- Copy the built dist/lzb-postr directory to your projects plugins folder.

## Usage

Create a lazy block that uses the postr control.


### Block Code
```
            'control_d919e94362' => array(
                'type' => 'postr',
                'name' => 'postr',
                'default' => '',
                'label' => 'post',
                'help' => '',
                'child_of' => '',
                'placement' => 'content',
                'width' => '100',
                'hide_if_not_selected' => 'false',
                'save_in_meta' => 'false',
                'save_in_meta_name' => '',
                'required' => 'false',
                'postr_types' => 'solution_partner,page',  // Specify which types to filter on OR blank for all.
                'placeholder' => '',
                'characters_limit' => '',
                'postr_custom_attribute' => 'solution_partner',
            ),
```

### Template

In the template you have only the name of the post and the id.

Make some handlebars helpers or use PHP to load the post based on id.

```
// Handler Bars
{{controlname.id}} - show the id of the chosen post.

// PHP
<?= $attributes['controlname']['id'] ?>

// PHP more interesting...
<?php
    $postId = $attributes['controlname']['id'];
    $post = get_post($postId);
?>

<p>
    <?= $post->post_title ?>
</p>
```

## Screenshot

![Screenshot](sample.png)