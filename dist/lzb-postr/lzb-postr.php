<?php
/**
 * Plugin Name:  Lazy Blocks: Post Reference Control
 * Description:  Small plugin to enable a Post Reference Control inside of a Lazy Block.
 * Plugin URI:   https://www.contentcoffee.com
 * Version:      1.0.0
 * Author:       Content and Coffee
 * Author URI:   https://www.contentcoffee.com
 * License:      GPLv2 or later
 * License URI:  https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:  project
 *
 * @package lzb-postr
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Project_Lzb_Plugin_postr Class
 */
class Project_Lzb_Plugin_postr {

    /**
     * Plugin Path.
     *
     * @var string
     */
    public static $plugin_path;

    /**
     * Plugin URL.
     *
     * @var string
     */
    public static $plugin_url;

    /**
     * Project_Lzb_Plugin_postr constructor.
     */
    public function __construct() {}

    /**
     * Init.
     */
    public static function init() {
        add_action( 'init', array( 'Project_Lzb_Plugin_postr', 'plugins_loaded' ), 11 );
    }

    /**
     * Init of LazyBlocks available.
     */
    public static function plugins_loaded() {
        if ( ! class_exists( 'LazyBlocks' ) ) {
            return;
        }

        self::$plugin_path = plugin_dir_path( __FILE__ );
        self::$plugin_url  = plugin_dir_url( __FILE__ );

        // Translations.
        load_plugin_textdomain( 'project', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

        // Include control.
        include_once self::$plugin_path . '/controls/postr.php';
    }
}

Project_Lzb_Plugin_postr::init();
