/**
 * Script for Post Reference Control
 */
import axios from 'axios';

const { __ } = wp.i18n

const {
  Component,
} = wp.element

const {
  compose, withInstanceId,
} = wp.compose

const {
  withSelect,
} = wp.data

const {
  addFilter,
} = wp.hooks

const {
  PanelBody, TextControl, BaseControl, Button,
} = wp.components

/**
 * Create a new control
 */
class PostrControl extends Component {
  constructor (...args) {
    super(...args)
    this.state = {
      suggestionItems: '',
      name: this.props.value && this.props.value.name
        ? this.props.value.name
        : null,
      id: this.props.value && this.props.value.id ? this.props.value.id : null,
    }
  }

  suggestionChosen (name, id) {
    this.setState({
      name: name, id: id,
    })
    this.props.onChange({ name: name, id: id })
    this.state.suggestionItems = ''
  }

  // Perform a search on the WP API
  performSearch (search) {
    let path = '/wp-json/wp/v2/search';
    let parms = {
      type: 'post',
      per_page: 5,
      search: search
    };

    // If we need to restrict types then we restrict types.
    if (this.props.types) {
      parms.subtype = this.props.types;
    }

    // Call the WP api and search for posts
    axios.get(path, {params: parms}).then(res => {
      let sr = res.data
      let si = '';
      // Map the results into list items for choosing
      si = sr.map((item, i) => {
        return (<div><Button onClick={() => this.suggestionChosen(item.title, item.id)}><strong>{item.title}</strong></Button> <em>({item.type}/{item.subtype})</em></div>);
      });

      // Set the state so that the results display.
      this.setState({
        suggestionItems: si,
      });

    }).catch(err => {

    });
  }

  // Clear the values in the control and send a onChange.
  clearValues () {
    this.setState({
      name: null, id: null,
    })
    this.props.onChange({ name: null, id: null })
  }

  // Display this monster!
  render () {
    const {
      label, value, help, types, onChange = () => {},
    } = this.props
    return (
      <div>
        <TextControl
          placeholder={'type to search'}
          onChange={v => this.performSearch(v)}
        />
        <div>Currently Selected: <strong>{this.state.name}</strong>
          {this.state.id ? <Button
            onClick={() => {this.clearValues()}}>X</Button> : ''}
        </div>
        <div className={'autocompleteresultsshown'}>
          {this.state.suggestionItems}
        </div>
      </div>)
  }
}

export default compose([
  withInstanceId, withSelect((select, ownProps) => {
  })])(PostrControl)

/**
 * Control render in editor.
 */
addFilter('lzb.editor.control.postr.render', 'lzb.editor', (render, props) => {

  return (
    <BaseControl
      label={props.data.label}
      help={props.data.help}
    >
      <PostrControl
        value={props.getValue()}
        types={props.data.postr_types}
        onChange={props.onChange}
      />
    </BaseControl>)
})

/**
 * getValue filter in editor.
 */
addFilter('lzb.editor.control.postr.getValue', 'lzb.editor', (value) => {
  // change string value to array.
  if ('string' === typeof value) {
    try {
      value = JSON.parse(decodeURI(value))
    }
    catch (e) {
      value = []
    }
  }
  return value
})

/**
 * updateValue filter in editor.
 */
addFilter('lzb.editor.control.postr.updateValue', 'lzb.editor', (value) => {
  // change array value to string.
  if ('object' === typeof value || Array.isArray(value)) {
    value = encodeURI(JSON.stringify(value))
  }

  return value
})

/**
 * Control settings render in constructor.
 */
addFilter('lzb.constructor.control.postr.settings', 'lzb.constructor',
  (render, props) => {
    const {
      updateData, data,
    } = props

    return (<PanelBody>
      <TextControl
        label={'Type(s,)'}
        value={data.postr_types}
        onChange={(val) => {
          updateData({
            postr_types: val,
          })
        }}
      />
    </PanelBody>)
  })
